



# Ansible openstack.cloud modules DOC которые были использованы в проекте
# Общий список всех модулей
https://docs.ansible.com/ansible/latest/collections/openstack/cloud/index.html

Создание нового инстанса (виртуальной машины) для проекта
https://docs.ansible.com/ansible/latest/collections/openstack/cloud/server_module.html#ansible-collections-openstack-cloud-server-module

Добавление в текущий инвентарь согласно созданного инстанаса. У нас используется для добавления инстанса в нужную нам группу для последующего развертывания ролей и playbook
https://docs.ansible.com/ansible/latest/collections/ansible/builtin/add_host_module.html

Создание ключей в проекте в облаке из ваших публичных ключей
https://docs.ansible.com/ansible/latest/collections/openstack/cloud/keypair_info_module.html#ansible-collections-openstack-cloud-keypair-info-module

Создание сетей в проекте
https://docs.ansible.com/ansible/latest/collections/openstack/cloud/network_module.html#ansible-collections-openstack-cloud-network-module

Создание подсетей
https://docs.ansible.com/ansible/latest/collections/openstack/cloud/subnet_module.html#ansible-collections-openstack-cloud-subnet-module

Создание роутера в сети для доступа к внешней сети и выдаче плавающий IP
https://docs.ansible.com/ansible/latest/collections/openstack/cloud/router_module.html#ansible-collections-openstack-cloud-router-module

Создание новой группы для настройки firewall
https://docs.ansible.com/ansible/latest/collections/openstack/cloud/security_group_module.html#ansible-collections-openstack-cloud-security-group-module

Создание правил для групп безопасности
https://docs.ansible.com/ansible/latest/collections/openstack/cloud/security_group_rule_module.html#ansible-collections-openstack-cloud-security-group-rule-module

Создание балансировщика средствами самого облачного сервиса MCS у на пока не заработал. Делаем через NGINX.
https://docs.ansible.com/ansible/latest/collections/openstack/cloud/loadbalancer_module.html#ansible-collections-openstack-cloud-loadbalancer-module

Управление серверами инстансами (stop start pause unpause)
https://docs.ansible.com/ansible/latest/collections/openstack/cloud/server_action_module.html#examples
