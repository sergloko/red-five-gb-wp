для развертывания инфораструктуры на mCS необходимо выполнить ряд требований 
ОС Ubuntu 20.04
python >=3.6
Ansible >=2.12
ansible-galaxy collection openstack.cloud

для ускорения установки окружения для развертывания можно использовать готовый скрипт
./scripts/requirements.sh
